# This file uses the python plotting library "plotly"
# to generate a number of useful histograms from the provided "results.csv" file

import csv
import plotly.offline as offline
import plotly.graph_objs as go

# Takes a Decimal & an Array as input, Returns a count of how many
# values in the array are smaller than the given decimal
def count_values_smaller_then_t(threshold, array):
    total = 0
    for x in array:
        if x < threshold:
            total = total + 1
    return total


# Takes a Decimal & an Array as input, Returns a count of how many
# values in the array are greater than or equal to the given decimal
def count_values_larger_then_t(threshold, array):
    total = 0
    for x in array:
        if x >= threshold:
            total = total + 1
    return total


# Array to store the scores of mated pairs (i.e. a mated value of 1)
matedArray = []
# Array to store the scores of unmated pairs (i.e. a mated value of 0)
unmatedArray = []
# track the number of rows read
rowNbr = 0

# open & read from csv file
with open('results.csv', 'r') as results:
    resultsReader = csv.reader(results, dialect='excel')

    for row in resultsReader:
        # Skip the header of the csv
        if rowNbr >= 1:
            isMated = int(row[3])
            if isMated:
                # append score to mated array
                matedArray.append(float(row[2]))
            else:
                # append score to unmated array
                unmatedArray.append(float(row[2]))

        rowNbr = rowNbr + 1

# Next we will find the False Non Match Rate (fnmr) & the False Match Rate (fmr)
sortedMatedArray = sorted(matedArray)
sortedUnmatedArray = sorted(unmatedArray)
# Array of Threshold values
tArray = []
# Array of False Non Match Rates
fnmrArray = []
# Array of False Match Rates
fmrArray = []

# set the threshold to be increments of 0.02
for t100 in range(0, 102, 2):
    t = t100 / 100
    tArray.append(t)

    # fnmr = the percentage of mated results that are smaller then the given threshold
    fnmr = round(count_values_smaller_then_t(t, sortedMatedArray) / len(sortedMatedArray), 2) * 100
    fnmrArray.append(fnmr)

    # fmr = the percentage of unmated results that are equal to or greater then the given threshold
    fmr = round(count_values_larger_then_t(t, sortedUnmatedArray) / len(sortedUnmatedArray), 2) * 100
    fmrArray.append(fmr)


# ------ Plotting Section -------

# Styling for the "Mated Histogram" we are about to plot
matedData = go.Histogram(
    x=matedArray,
    name='mated',
    xbins=dict(
        start=0.0,
        end=1.0,
        size=0.02
    ),
    marker=dict(
        color='#EB89B5',
        line=dict(
            width=0.5
        )
    ),
    opacity=0.75
)

# Styling for the "UnMated Histogram" we are about to plot
unmatedData = go.Histogram(
    x=unmatedArray,
    name='unmated',
    xbins=dict(
        start=0.0,
        end=1.0,
        size=0.02
    ),
    marker=dict(
        color='#FFD7E9',
        line=dict(
            width=0.5
        )
    ),
    opacity=0.75
)

# plot the mated histogram, will open in browser & also download a .jpeg copy of graph
offline.plot([matedData], show_link=False, filename="matedHistogram.html", image='jpeg', image_filename="matedHistogram")

# plot the unmated histogram, will open in browser & also download a .jpeg copy of graph
offline.plot([unmatedData], show_link=False, filename="unmatedHistogram.html", image='jpeg', image_filename="unmatedHistogram")


# Styling for the "Combined Histogram" graph that we are about to plot
layout = go.Layout(
    title='Combined Results',
    xaxis=dict(
        title='Value'
    ),
    yaxis=dict(
        title='Count'
    ),
    bargap=0.2,
    bargroupgap=0.1
)
# Combining the 2 data series with our custom layout
fig = go.Figure(data=[matedData, unmatedData], layout=layout)

# plot a histogram that combines our two data series, will open in browser & also download a .jpeg copy of graph
# However isn't the prettiest to look at due to the unmated data series being much larger than the mated one
offline.plot(fig, show_link=False, filename="combinedHistogram.html", image='jpeg', image_filename="combinedHistogram")

# A fix for this is to "Normalise" the results
# # by displaying a "percentage of total occurrences" on the Y axis, rather than a "Count"
matedData.histnorm = 'percent'
unmatedData.histnorm = 'percent'

normalisedLayout = go.Layout(
    title='Combined & Normalised Results',
    xaxis=dict(
        title='Value'
    ),
    yaxis=dict(
        title='Percentage Count'
    ),
    bargap=0.2,
    bargroupgap=0.1
)

normFig = go.Figure(data=[matedData, unmatedData], layout=normalisedLayout)

# Plot a histogram that combines our two data series with a normalised Y axis,
# will open in browser & also download a .jpeg copy of graph
offline.plot(normFig, show_link=False, filename="combinedNormalisedHistogram.html", image='jpeg', image_filename="combinedNormalisedHistogram")


# Now to plot the fnmr & fmr rates obtained earlier so that we can identify the EER (Equal Error Rate)

# Styling for the fnmr
fnmrSeries = go.Scatter(
    x=tArray,
    y=fnmrArray,
    mode='lines',
    name='FNMR'
)

# Styling for the fmr
fmrSeries = go.Scatter(
    x=tArray,
    y=fmrArray,
    mode='lines',
    name='FMR'
)

lineGraphLayout = dict(
    xaxis=dict(title='Threshold'),
    yaxis=dict(title='Error Rate (%)')
)

lineFig = go.Figure(data=[fnmrSeries, fmrSeries], layout=lineGraphLayout)

# Plot a line graph showing the equal error rate (i.e. the point where the fnmr & fmr intersect),
# will open in browser & also download a .jpeg copy of graph
offline.plot(lineFig, show_link=False, filename="EER.html", image='jpeg', image_filename="EER")

# As can be seen from the above graph, the equal error rate appears to occur when the threshold is set to 0.47
# this results in an error rate of approximately 0.5% for both false matches & false non-matches
