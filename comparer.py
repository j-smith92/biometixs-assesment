# This python script generates a .csv file called 'results.csv'
# that compares the various images from the provided "FeretMedium" data set

import os
import face_recognition
import csv


# Takes 2 file names as input and returns if their ID's match
def checkmating(prodename, subjectname):
    prodeid = prodename[:5]
    subjectid = subjectname[:5]
    if prodeid == subjectid:
        return 1
    else:
        return 0


# Set path to the image data set
imagePath = os.getcwd() + "/FeretMedium"
# Set path to the results csv
csvPath = os.getcwd() + "/results.csv"


# Remove .csv left over from previous execution
if os.path.isfile(csvPath):
    os.remove(csvPath)

# Create CSV file
with open(csvPath, 'w') as results:
    resultsWriter = csv.writer(results, dialect='excel')
    resultsWriter.writerow(['Prode_ID', 'Subject_ID', 'Match_Score', 'Mated?'])

    # Load the first face from the data set & get it's encoding
    with os.scandir(imagePath) as listOfProdes:
        for prode in listOfProdes:
            if prode.is_file() and prode.name.endswith(".jpg"):
                knownFaceImage = face_recognition.load_image_file(prode.path)
                knownFaceEncoding = face_recognition.face_encodings(knownFaceImage)
                if len(knownFaceEncoding) > 0:
                    knownFaceEncoding = knownFaceEncoding [0]

                    # Load a second face from the data set & get it's encoding
                    with os.scandir(imagePath) as listOfSubjects:
                        for subject in listOfSubjects:
                            if subject.is_file() and subject.name.endswith(".jpg"):
                                if prode.name != subject.name:
                                    unknownFaceImage = face_recognition.load_image_file(subject.path)
                                    unknownFaceEncoding = face_recognition.face_encodings(unknownFaceImage)
                                    if len(unknownFaceEncoding) > 0:
                                        unknownFaceEncoding = unknownFaceEncoding [0]

                                        # Compare the two face encodings
                                        score = face_recognition.face_distance([knownFaceEncoding], unknownFaceEncoding)
                                        score = 1 - score[0]
                                        # Check if the ID's match
                                        mated = checkmating(prode.name, subject.name)
                                        # Write results to CSV file
                                        resultsWriter.writerow([prode.name, subject.name, score, mated])

                                    else:
                                        print("Unable to identify a face in image file: \n" +
                                              prode.path + "\n"
                                              + "Skipping processing of this image \n")

                else:
                    print("Unable to identify a face in image file: \n" +
                          prode.path + "\n"
                          + "Skipping processing of this image \n")




